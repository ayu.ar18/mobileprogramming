import React, {Component} from 'react';
import { StyleSheet, Text, View, Image,ImageBackground, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <ImageBackground source={require("./assets/pink.jpg")} style={styles.bgcont}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/ay.jpg')} />
                    </View>
                    <Text style={styles.bio}>Ayumas Aura Krishnavarty</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Program study : Teknik Informatika </Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>kelas : Pagi A </Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Agama : Islam</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>TTL : Klaten,18 Januari 2000</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Alamat : Purwakarta</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Instagram : Ayu.ar18</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Facebook :-</Text>            
                </View>
                </ImageBackground>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#2F4F4F',
        flex:1,
        width:'100%',
        height:'100%'
    },

    bgcont:{
        flex:1,
        resizeMode:'cover',
        justifyContent:'center',
        width:'100%',
        height:'100%',
        alignItems:'center',
      },

    bio:{
        textAlign:"center",
    marginTop:5,
    marginBottom:5,
    fontSize:18,
    fontFamily:'serif',
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 68,
        marginBottom:'10%',
    },


});