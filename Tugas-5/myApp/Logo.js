import React, {Component} from 'react';
import { StyleSheet, ImageBackground, Text, View, Image } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style={styles.container}>

            <Image style={styles.image} source={require("./assets/1509570.png")}/>
                    <Text style={styles.logoText}>HELLO, </Text>
                    <Text style={styles.logoText}>WELCOME TO DREAM APP</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }, 
    logoText: {
        fontSize: 20,
        color: '#000',
        fontFamily:'monospace',
        alignItems:'center',
        textAlign:'center',
    },
    image: {
        width:90,
        height:90,
        marginTop:'80%'
      },
});
